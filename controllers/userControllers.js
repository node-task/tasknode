const user = require('../models/userModel');
const message = require('../commonFile/commonFunction');
const bcrypt = require('bcrypt-nodejs');
var otpGenerator = require('otp-generator')
const constant = require('../commonFile/constant.json')
var otp;

module.exports = {
    addUser: (req, res) => {
        try {
            var details = {};
            if (!req.body.email || !req.body.password) {
                details.status = constant.errorCode;
                details.statusMsg = constant.errorMsg;
                details.message = constant.required_parameter_missing;
                return res.status(constant.successCode).send(details).end();
            }
            else {
                var query = { $and: [{ status: { $in: ["ACTIVE", "BLOCk"] } }, { email: req.body.email }] }
                user.find(query).catch(err => {
                    console.log("error in find", err)
                    details.status = constant.Internal_server_error;
                    details.statusMsg = constant.error;
                    return res.status(constant.successCode).send(details).end();
                }).then(userDetails => {
                    console.log("userDetails", userDetails)
                    if (!userDetails || !userDetails.length) {
                        req.body.password = bcrypt.hashSync(req.body.password);
                        var data = new user(req.body);
                        data.save((error, userData) => {
                            if (error) {
                                details.status = constant.Internal_server_error;
                                details.statusMsg = constant.error;
                                details.result = userData;
                                return res.status(constant.successCode).send(details).end();
                            }
                            else {
                                details.status = constant.successCode;
                                details.statusMsg = constant.successfully_usercreate;
                                details.result = userData;
                                return res.status(constant.successCode).send(details).end();
                            }
                        })
                    } else {
                        details.status = constant.successCode;
                        details.statusMsg = constant.email_exists;
                        details.result = userDetails;
                        return res.status(constant.successCode).send(details).end();
                    }
                })
            }
        }
        catch {
            console.log("exception area")
        }
    },
    updateUserDetails: (req, res) => {
        try {
            var details = {};
            if (!req.body.email) {
                details.status = constant.errorCode;
                details.statusMsg = constant.errorMsg;
                details.message = constant.required_parameter_missing;
                return res.status(constant.successCode).send(details).end();
            } else {
                var query = { $and: [{ status: { $in: ["ACTIVE", "BLOCk"] } }, { email: req.body.email }] }
                user.findOne(query).catch(err => {
                    details.status = constant.Internal_server_error;
                    details.statusMsg = constant.error;
                    return res.status(constant.successCode).send(details).end();
                }).then(userFound => {
                    if (userFound) {
                        let object = {
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            mobileNo: req.body.mobileNo,
                            countryCode: req.body.countryCode,
                            mergeContact: req.body.countryCode + req.body.mobileNo
                        }
                        user.findOneAndUpdate({ email: req.body.email, status: "ACTIVE" }, { $set: object }, { new: true }).catch(error => {
                            console.log("error found")
                            details.status = constant.Internal_server_error;
                            details.statusMsg = constant.error;
                            return res.status(constant.successCode).send(details).end();
                        }).then(userData => {
                            console.log("updated result", userData)
                            details.status = constant.successCode;
                            details.statusMsg = constant.successfully_userupdate;
                            details.result = userData;
                            return res.status(constant.successCode).send(details).end();
                        })
                    }
                    else {
                        details.status = constant.successCode;
                        details.statusMsg = constant._usernotexists;
                        details.result = {};
                        return res.status(constant.successCode).send(details).end();
                    }
                })
            }
        } catch {
            console.log("in catch block")
        }
    },
    deleteUser: async (req, res) => {
        try {
            var details = {};
            if (!req.body.email) {
                details.status = constant.errorCode;
                details.statusMsg = constant.errorMsg;
                details.message = constant.required_parameter_missing;
                return res.status(constant.successCode).send(details).end();
            } else {
                var query = { $and: [{ status: { $in: ["ACTIVE", "BLOCK"] } }, { email: req.body.email }] }
                await user.findOne(query).catch(err => {
                    details.status = constant.Internal_server_error;
                    details.statusMsg = constant.error;
                    return res.status(constant.successCode).send(details).end();
                }).then(userFound => {
                    if (userFound) {
                        user.findOneAndUpdate({ email: req.body.email, status: "ACTIVE" }, { $set: { status: "DELETE" } }, { new: true }).catch(error => {
                            console.log("error found")
                            details.status = constant.Internal_server_error;
                            details.statusMsg = constant.error;
                            details.result = {};
                            return res.status(constant.successCode).send(details).end();
                        }).then(userData => {
                            console.log("updated result", userData)
                            details.status = constant.successCode;
                            details.statusMsg = constant.successfully_userdelete;
                            details.result = userData;
                            return res.status(constant.successCode).send(details).end();
                        })
                    } else {
                        details.status = constant.successCode;
                        details.statusMsg = constant._usernotexists;
                        details.result = {};
                        return res.status(constant.successCode).send(details).end();
                    }
                })
            }
        } catch {
            console.log("error catch block")
        }
    },
    getUserDetails: (req,res)=>{
        var details={};
        if(!req.body.email){
            details.status = constant.errorCode;
            details.statusMsg = constant.errorMsg;
            details.message = constant.required_parameter_missing;
            return res.status(constant.successCode).send(details).end(); 
        }
        else{
            var query = { $and: [{ status: { $in: ["ACTIVE", "BLOCK"] } }, { email: req.body.email }] }
             user.findOne(query).catch(err => {
                details.status = constant.Internal_server_error;
                details.statusMsg = constant.error;
                return res.status(constant.successCode).send(details).end();
             }).then(userData=>{
                details.status = constant.successCode;
                details.statusMsg = constant.userDetails;
                details.result = userData;
                return res.status(constant.successCode).send(details).end();
             })
        }
    },
}