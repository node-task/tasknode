var router = require('express').Router();
var controller = require('../controllers/userControllers');

//var authController=require('../controllers/auth');
router.post('/addUser', controller.addUser);
router.post('/updateUserDetails', controller.updateUserDetails);
router.post('/deleteUser', controller.deleteUser);
router.post('/getUserDetails', controller.getUserDetails);
// router.post('/forgotPassword', controller.forgotPassword);
module.exports = router;