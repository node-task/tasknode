const express = require('express')
const app = express()
const path = require('path');
const request = require('request');
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true, limit: '30mb' }))
app.use(bodyParser.json({ limit: "16mb" }))
//adding views dir on there.
app.use(bodyParser.json())
app.use('/assets', express.static('public'))
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, "views"));

require('dotenv').config({path: __dirname + '/.env'})

app.get('/', (req, res) => {
    console.log("hello world!")
    res.json("hello here..")
    // res.render("listing.ejs")
})
app.use('/api/v1', require('./routes/userRoutes'))
const db = require('./mongodb')
let port= process.env.PORT || 8000;
app.listen(port, () => {
    console.log(`app is listening on port ${port}`)     
})