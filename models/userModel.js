const mongoose = require('mongoose')
const schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
var user = new schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    mobileNo: {
        type: String,
    },
    countryCode: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    mergeContact: {
        type: String
    },
    password: {
        type: String
    },
    verifyOtp: {
        type: Boolean,
        default: false
    },
    userType: {
        type: String,
        enum: ["VENDOR", "USER"],
        default: "USER"
    },
    status: {
        type: String,
        default: "ACTIVE",
        enum: ["ACTIVE", "BLOCK", "DELETE"]
    },
    otp: {
        type: String
    },
    sendingTime: {
        type: Date
    },
    enteringTime: {
        type: Date
    }
}, { timestamps: true })
module.exports = mongoose.model('user', user);
(function init() {
    let object = {
        firstName: 'Pankaj',
        lastName: 'Gururani',
        mobileNo: "9917073613",
        countryCode: "+91",
        password:bcrypt.hashSync('Pankaj1997'),
        email: 'pankaj@yopmail.com'
    }
    mongoose.model("user", user).findOne({ email: "pankaj@yopmail.com" }, (err, result) => {
        if (err) {
            console.log("manager creation at findOne error--> ", err);
        }
        else if (!result) {
            mongoose.model("user", user).create(object, (err, success) => {
                if (err) {
                    console.log("error occered in admin creation")

                } else {
                    console.log(" admin creation sucess")
                }
            })
        }
        else {
            console.log("Default Admin.", result)
        }
    })  
})();
